
abstract class Shape{
    public abstract void display(int a, int b);
    public void Dis() {
        System.out.println("It is a geometric figure");
    };
}

class Area extends Shape{
    public void display(int a,int b){
        System.out.println("The Area is "+a*b);
    }
}

class Perimeter extends Shape{
    public void display(int a,int b){
        System.out.println("The Perimeter is "+2*(a+b));
    }
}

public class Partial{
    public static void main(String[] args){
        Area area = new Area();
        Perimeter peri = new Perimeter();
        area.display(3, 4);
        area.Dis();
        peri.display(3, 4);
    }
}
