 #include <iostream>

class Point_add {
public:
    int x,y;
    Point_add(int x = 0, int y = 0) {
        this->x = x;
        this->y = y;
    }

    Point_add operator+(const Point_add& new_point) {
        Point_add result;
        result.x = this->x + new_point.x;
        result.y = this->y + new_point.y;
        return result;
    }

    void display() {
        std::cout << "(" << x << ", " << y << ")" << std::endl;
    }

};

int main() {
    Point_add v1(2, 4);
    Point_add v2(3, 6);
    Point_add v3 = v1 + v2;  // Operator overloading
    v3.display();         // Output: (5, 10)
    return 0;
}