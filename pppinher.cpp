
#include <iostream>
using namespace std;
// Define the base class
class Animal {
public:
    void eat() {
        cout << "Animal is eating" << endl;
    }
    void sleep() {
        cout << "Animal is sleeping" << endl;
    }
    void move() {
        cout << "Animal is moving" << endl;
    }
private:
    void breath(){
        cout << "Animal is breathing" << endl;
    }
};
// Define the derived class using private inheritance
class DogPrivate : private Animal {
public:
    void bark() {
        cout << "Dog is barking" << endl;
    }
    void doDogThings() {
     //breath(); Not allowed, since breathe is private in Animal
        eat();
        sleep();
        move();
        bark();
    }
};
// Define the derived class using protected inheritance
class DogProtected : protected Animal {
public:
    void bark() {
        cout << "Dog is barking" << endl;
    }
    void doDogThings() {
     // Not allowed, since breathe is private in Animal
        eat();
        sleep();
        move();
        bark();
    }
};
// Define the derived class using public inheritance
class DogPublic : public Animal {
public:
    void bark() {
        cout << "Dog is barking" << endl;
    }

    void doDogThings() {
        //breathe()// Not allowed, since breathe is private in Animal
        eat();
        sleep();
        move();
        bark();
    }
};
int main() {
    // Create an object of each class and test their functions
    DogPrivate privateDog;
    DogProtected protectedDog;
    DogPublic publicDog;

    cout << "Testing DogPrivate class:" << endl;
    privateDog.doDogThings(); // Not allowed, since Animal's private member breathe() is inaccessible

    cout << "Testing DogProtected class:" << endl;
    protectedDog.doDogThings(); // Not allowed, since Animal's private member breathe() is inaccessible

    cout << "Testing DogPublic class:" << endl;
    publicDog.doDogThings(); // All functions are accessible

    return 0;
}
