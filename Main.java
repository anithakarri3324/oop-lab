class Greetings {
  // parent class
  String name;
  public void greet() {
    System.out.println("Namasthey");
  }
}
// inherit from Greetings
class Intro extends Greetings {
  // new method in subclass
  public void display() {
    System.out.println("My name is " + name);
  }
}
class Main {
  public static void main(String[] args) {
    //object of the subclass
    Intro details = new Intro();
    details.greet();
    details.name = "Rohu";
    details.display();
  }
}